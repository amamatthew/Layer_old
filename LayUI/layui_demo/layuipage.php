<?php
  /**
    *获取总页数  
    * @author 浪子  <564014151@qq.com>
	  * @time  2016-12-29 18:00
	  * @QQ交流群 515187709
    */  
   $arr=array(
     array('id'=>1,'city'=>'北京'),
     array('id'=>2,'city'=>'上海'),
     array('id'=>3,'city'=>'广州'),
     array('id'=>4,'city'=>'深圳'),
     array('id'=>5,'city'=>'杭州')
     );
    $p=1;//每页显示1条数据
    $allnum=round(count($arr)/$p);//总共的页数
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link rel="stylesheet" href="layui/css/layui.css"  media="all">
<script language="JavaScript" src="layui/layui.js"></script>
</head>
<body>
    <ul id="view">
      <li>北京</li>
      <li>上海</li>
      <li>广州</li>
      <li>深圳</li>
      <li>杭州</li>
    </ul>
    <div id="page"></div>
    <script>
    pagelist();//调用方法
    function  pagelist()
    {
      layui.use(['jquery','laypage'], function(){
      var $ = layui.jquery //重点处
         ,laypage = layui.laypage;
          laypage({
              cont: 'page',
              pages: <?php echo $allnum?> ,
              skin: '#1E9FFF',
              jump: function(e) { //触发分页后的回调
                  var url="pagedata.php";
                  $.ajax({
                      type: 'POST',
                      url: url,
                      data:{page: e.curr},
                      dataType: "json",
                      success:function(res){
                          document.getElementById('view').innerHTML = res.content;
                      }
                  });
              }
          });
      });
    }
    </script>
</body>
</html>